package threesolid;

public class SuperWorker implements IWorkable, IFeedable{
	public void work() {
		System.out.println("(Work method from SuperWorker) I really do be working so much more now wow!");
	}

	public void eat() {
		System.out.println("(Eat method from SuperWorker) I love to eat food, haha!");
	}
}