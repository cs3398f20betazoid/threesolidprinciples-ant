package threesolid;

public class Worker implements IWorkable, IFeedable{
    public void work(){
              System.out.println("Worker is working...");
    }

    public void eat(){
              System.out.println("Worker is eating...");
    }

}