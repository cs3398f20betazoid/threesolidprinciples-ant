package threesolid;

public class Manager {

    IWorkable worker;

    public Manager () {

    }

	public void setWorker(IWorkable w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
}
