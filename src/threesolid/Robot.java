package threesolid;

public class Robot implements IWorkable  {

    public void work (){
        System.out.print("(work() method from Robot class) Beep boop doing my job...");
    }

}