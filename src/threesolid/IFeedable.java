package threesolid;

public interface IFeedable {
    public void eat();
}
