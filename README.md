# ThreeSolidPrinciples 
Johana D., Blair T., Mary L., Blane R., Tristan W.

## Preface:
There were some complications that arose (i.e. git / merging), so there may be a discrepancy between who wrote their assigned class as listed below and who committed the files.

## File contributors and SOLID methods used:
* **Johana D.**: SuperWorker.java; this file implements the _open-closed SOLID principle_ & _Single Responsibility Principle_ because the file is open for extension since it is built off of implemented files. Not to mention, this is Single Responsibility because this SuperWorker file is similar to that of Worker, but since it has a further functionality, it has it's own class.
* **Blane R.**: IWorkable & IFeedable.java; these files use the SOLID principle of _Interface Segregation Principle_ as it is spliting up one general interface into many specific interfaces so that the interfaces have a specific focus.
* **Mary L.**: Manager & Robot.java; The Manager class represents an _open-closed principle_ because the functionality of this class will not change unless the classes being called within the function are updated. The Robot class is similar to that of Worker and SuperWorker files where it uses a _Single responsibility principle_ since it's an extension of a characted in this program, but has it's own class with its own responsibilities.
* **Blair R.**: Worker.java; The Worker class represents the SOLID principle of _Interface Segregation Principle_ as it is spliting up one general interface into many specific interfaces so that the interfaces have a specific focus.
* **Tristan W.**: ThreeSolidMain.java; No SOLID methods, as it is testing the class implementations.

## SOLID Software Principles (for reference):
* Single Responsibility Principle: A class should only have a single responsibility, that is, it should have one (or only a few) reasons to change.
* Open-closed Principle: Software entities should be open for extension, but closed for modification.
* Interface Segregation principle: Many client-specific interfaces are better than one general-purpose interface.